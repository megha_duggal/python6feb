student=[{
    'name':'Megha',
    'roll_no':'1',                        #Dictionary is mutable
    'class':'python'
    'subjects':['HTML','Css','Python','JS']
}]
# print(student['name'])
# print(student['roll_no'])
# print(type(student))
# print(type(student['roll_no']))
# print(student['subjects'][2])
print(type(student))