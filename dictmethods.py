d={}
# add elements in dictionary
d['name']='Shinchen'
d['last_name']='Nobara'
d.update({'pet_name':'Sheero'})
d.update({'Age':5})
# update values in dictionary
d['name']='Harry'
print(len(d))
del d['pet_name']
#Count no. of elements
print(len(d))
print(d)
#Search for a key
print('name' in d)
print('name' in d.keys())
print('name' in d.values())
print('Harry' in d.values())
print('salary' in d)
# Create a new dictionary from set of keys
# newd={}
# t=('name','salary','leaves','age')
# newd= newd.fromkeys(t,100)
# print(newd)
#fetch keys,value and pairs
# print(d.keys())
# print(d.values())
# print(d.items())
#clear dictionary
#print(d)
#d.clear()
# print(d)
#copy a dictionary
n=d.copy()
print(n)